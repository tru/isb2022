#!/bin/sh
PATH=/sbin:/usr/sbin:/usr/bin:/bin

set -e
set -u

yum -y install patch || :
sed -i -e 's/^root.*postfix/root: tru\@pasteur.fr/' /etc/aliases 
grep tru@pasteur.fr /etc/aliases|| (echo 'root: tru@pasteur.fr'| tee -a /etc/aliases )
/usr/bin/newaliases
postconf | grep message_size_limit
postconf -e 'inet_protocols = ipv4'
postconf -e 'message_size_limit = 20480000' 
postconf -e 'relayhost = [smtp.pasteur.fr]'
postconf -e 'mydestination = '
postconf -e 'virtual_alias_maps = regexp:/etc/postfix/virtual'
cd /etc/postfix && curl  https://gitlab.pasteur.fr/tru/isb2022/-/raw/main/postfix-virtual-isb2022.patch  | patch && \
postmap virtual
/sbin/service postfix restart
postconf | grep message_size_limit
date| mail -s `hostname` tru
date| mail -s `hostname` tru@`hostname`
echo | mail -s root:`hostname` root

