#!/bin/sh
PATH=/sbin:/usr/sbin:/usr/bin:/bin

set -e
set -u

tbindir=$(mktemp -d)
trap "rm -rf \"$tbindir\"" TERM INT EXIT

N=cuda_11.7.0_515.43.04_linux.run
V=`echo ${N}| sed 's/^cuda_//g;s/_linux.run$//g'`
# scp /c7/home/tru/sources/cuda/cuda_11.7.0_515.43.04_linux.run admin@ISB2022:
A=~/${N}
D=/ISB2022/cuda-$V
echo $N
echo $V
echo $A
echo $D
if [ -f $A ] && [ ! -d $D ]; then
echo
echo installing $A into $D
mkdir -p $D && \
sh $A \
--toolkit \
--toolkitpath=$D \
--silent
fi
mkdir -p /ISB2022/modulefiles/cuda || :
cat <<EOF_cuda > /ISB2022/modulefiles/cuda-11.7.0_515.43.04
#%Module1.0#####################################################################
module-whatis   "adds /ISB2022/cuda-11.7.0_515.43.04/bin to your PATH environment variable"

prepend-path    PATH    /ISB2022/cuda-11.7.0_515.43.04/bin
prepend-path    MANPATH /ISB2022/cuda-11.7.0_515.43.04/share/man

prepend-path -d " " CFLAGS "-I/ISB2022/cuda-11.7.0_515.43.04/include"
prepend-path -d " " CXXFLAGS "-I/ISB2022/cuda-11.7.0_515.43.04/include"
prepend-path -d " " CPPFLAGS "-I/ISB2022/cuda-11.7.0_515.43.04/include"

prepend-path -d " " LDFLAGS "-L/ISB2022/cuda-11.7.0_515.43.04/lib64 -L/ISB2022/cuda-11.7.0_515.43.04/lib -L/usr/lib64/nvidia -L/usr/lib/nvidia"
prepend-path    LD_RUN_PATH /ISB2022/cuda-11.7.0_515.43.04/lib64:/ISB2022/cuda-11.7.0_515.43.04/lib:/usr/lib64/nvidia:/usr/lib/nvidia
prepend-path    LD_LIBRARY_PATH /ISB2022/cuda-11.7.0_515.43.04/lib64:/ISB2022/cuda-11.7.0_515.43.04/lib:/usr/lib64/nvidia:/usr/lib/nvidia

setenv CUDA_INSTALL_PATH /ISB2022/cuda-11.7.0_515.43.04
setenv CUDA_HOME         /ISB2022/cuda-11.7.0_515.43.04
setenv CUDA_PATH         /ISB2022/cuda-11.7.0_515.43.04
prepend-path    EXTRA_LDFLAGS "-L/usr/lib64/nvidia"
prepend-path    PATH    /ISB2022/cuda-11.7.0_515.43.04/samples/bin/x86_64/linux/release

EOF_cuda

