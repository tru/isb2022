#!/bin/bash
PATH=/sbin:/usr/sbin:/usr/bin:/bin

set -e
set -u

tbindir=$(mktemp -d)
trap "rm -rf \"$tbindir\"" TERM INT EXIT
module purge
module add mpi/openmpi-x86_64
module use /ISB2022/modulefiles
module add cuda/11.7.0_515.43.04

git clone https://github.com/3dem/relion.git
cd relion
git checkout ver4.0
git pull
mkdir build
cd build
cmake3 ..  -DCMAKE_INSTALL_PREFIX=/ISB2022/relion4 
make -j 8
make install



