#!/bin/sh
# curl https://gitlab.pasteur.fr/tru/ISB2022/raw/master/c7-sshd_config.sh | sh
PATH=/sbin:/bin:/usr/sbin:/usr/bin
export PATH
# sshd
/usr/bin/sed -i -e 's/^PermitRootLogin.*/PermitRootLogin no/g' /etc/ssh/sshd_config
/usr/bin/sed -i -e 's/^PasswordAuthentication.*/PasswordAuthentication no/g' /etc/ssh/sshd_config
#
# fix selinux permissions
/sbin/restorecon -rv /etc/ssh


