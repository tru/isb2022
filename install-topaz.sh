#!/bin/bash
PATH=/sbin:/usr/sbin:/usr/bin:/bin

set -e
set -u

tbindir=$(mktemp -d)
trap "rm -rf \"$tbindir\"" TERM INT EXIT
source /ISB2022/bin/enable-miniconda3.sh
conda create -n topaz python=3.6
conda activate topaz 
conda install topaz cudatoolkit=11.3 -c tbepler -c pytorch
