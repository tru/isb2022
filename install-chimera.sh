#!/bin/sh
PATH=/sbin:/usr/sbin:/usr/bin:/bin

set -e
set -u

tbindir=$(mktemp -d)
trap "rm -rf \"$tbindir\"" TERM INT EXIT

stty erase 
echo choose to install to /ISB2022/chimera-1.16-linux_x86_64
./chimera-1.16-linux_x86_64.bin
