#!/bin/sh
PATH=/sbin:/usr/sbin:/usr/bin:/bin

set -e
set -u

yum -y install https://www.elrepo.org/elrepo-release-7.0-4.el7.elrepo.noarch.rpm
yum -y install nvidia-detect kmod-nvidia

# specific to AIO-7460
# keep xorg.conf for intel as the nvidia discrete card is not connected to the internal screen...

#/etc/default/grub
# GRUB_CMDLINE_LINUX="spectre_v2=retpoline rd.lvm.lv=vgdepens/root rd.lvm.lv=vgdepens/swap rhgb quiet nouveau.modeset=0 rd.driver.blacklist=nouveau plymouth.ignore-udev"

sed -i -e 's|rhgb quiet||g' /etc/default/grub
#     [admin@ISB2022 ~]$ cat /etc/X11/xorg.conf
#     # /etc/X11/nvidia-xorg.conf provided by http://elrepo.org
#     
#     Section "Device"
#             Identifier  "Videocard0"
#             Driver      "nvidia"
#     EndSection
#     [admin@ISB2022 ~]$ md5sum /etc/X11/xorg.conf
#     64d087ba2431026b330a8220fe1ecbe3  /etc/X11/xorg.conf
echo '64d087ba2431026b330a8220fe1ecbe3  /etc/X11/xorg.conf'| md5sum -c && \
	rm /etc/X11/xorg.conf
