#!/bin/sh
PATH=/sbin:/usr/sbin:/usr/bin:/bin

set -e
set -u

tbindir=$(mktemp -d)
trap "rm -rf \"$tbindir\"" TERM INT EXIT

module purge
module use /ISB2022/modulefiles
module add cuda/11.7.0_515.43.04
cd $CUDA_HOME
clone https://github.com/nvidia/cuda-samples samples
cd samples
make -j 8
type deviceQuery
